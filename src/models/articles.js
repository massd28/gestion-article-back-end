var N1qlQuery = require("couchbase").N1qlQuery;
var bucket = require("../../app").bucket; 

function ArticleModel(){ }

ArticleModel.getAll = function(callback) {
    var statement = "SELECT META().id, post_title, publicationDate, post_content FROM " + 
                    "`"+bucket._name+"` " +
                    "WHERE type = \"article\";"
    var query = N1qlQuery.fromString( statement);
    bucket.query(query, function(error, result) {
        if(error) {
            return callback(error, null);
        }
        callback(null, result);
    });
};

ArticleModel.findById = function(id, callback) {
    var statement = "SELECT META().id, post_title, publicationDate, post_content, post_image FROM " + 
                    "`"+bucket._name+"` " +
                    " USE KEYS '"+id+"' WHERE type = \"article\";"
    var query = N1qlQuery.fromString( statement);
    bucket.query(query, function(error, result) {
        if(error) {
            return callback(error, null);
        }
        callback(null, result);
    });
};

ArticleModel.searchQuery = function(expression, callback) {
    var statement = "SELECT META().id, post_content, post_title, name, type, articles, publicationDate FROM `"+bucket._name+ 
                    "` AS q WHERE SEARCH(q, {"+
                    "\"match\": \""+expression+"\"," +
                    "\"fields\": [\"post_content\", \"name\", \"post_title\"],"+
                    "\"fuzziness\": 1});"
    var query = N1qlQuery.fromString( statement);
    bucket.query(query, function(error, result) {
        if(error) {
            return callback(error, null);
        }
        callback(null, result);
    });
};


module.exports = ArticleModel;