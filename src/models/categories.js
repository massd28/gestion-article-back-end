var N1qlQuery = require("couchbase").N1qlQuery;
var bucket = require("../../app").bucket; 

function CategoryModel(){ }

CategoryModel.getAll = function(callback) {
    var statement = "SELECT META().id, articles, name, type FROM " + 
                    "`"+bucket._name+"` " +
                    "WHERE type = \"categorie\";"
    var query = N1qlQuery.fromString( statement);
    bucket.query(query, function(error, result) {
        if(error) {
            return callback(error, null);
        }
        callback(null, result);
    });
};

CategoryModel.getById = function(id, callback) {
    var statement = "SELECT categorie.name, (SELECT META().id, post_title, publicationDate FROM `"
                    +bucket._name+"`  USE KEYS categorie.articles WHERE type=\"article\") AS articlec FROM `"
                    +bucket._name+"` AS categorie USE KEYS '"+ id +"' WHERE type =\"categorie\";"
    var query = N1qlQuery.fromString( statement);
    bucket.query(query, function(error, result) {
        if(error) {
            return callback(error, null);
        }
        callback(null, result);
    });
};


module.exports = CategoryModel;