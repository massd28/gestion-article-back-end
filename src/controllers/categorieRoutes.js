var CategoryModel = require("../models/categories");

var router = function(app) {
 
    app.get("/categories", function(request, response) {
        CategoryModel.getAll(function(error, result) {
            if(error) {
                return response.status(401).send({ "success": false, "message": error});
            }
            response.send(result);
        });
    });
 
    app.get("/categorie/:id", function(request, response) {
        CategoryModel.getById(request.params.id, function(error, result) {
            if(error) {
                return response.status(401).send({ "success": false, "message": error});
            }
            response.send(result[0]);
        });
    });
 
}
 
module.exports = router;