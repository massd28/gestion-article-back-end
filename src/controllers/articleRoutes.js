var ArticleModel = require("../models/articles");

var router = function(app) {
 
    app.get("/articles", function(request, response) {
        ArticleModel.getAll(function(error, result) {
            if(error) {
                return response.status(401).send({ "success": false, "message": error});
            }
            response.send(result);
        });
    });
 
    app.get("/article/:id", function(request, response) {
        ArticleModel.findById(request.params.id, function(error, result) {
            if(error) {
                return response.status(401).send({ "success": false, "message": error});
            }
            response.send(result[0]);
        });
    });

    app.get("/search/:expression", function(request, response) {
        ArticleModel.searchQuery(request.params.expression, function(error, result) {
            if(error) {
                return response.status(401).send({ "success": false, "message": error});
            }
            response.send(result);
        });
    });
 
}
 
module.exports = router;