var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var couchbase = require("couchbase");
//var N1qlQuery = couchbase.N1qlQuery;
const dotenv = require("dotenv");
dotenv.config();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true}));

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
  });

var cluster = new couchbase.Cluster(process.env.CLUSTER);
cluster.authenticate(process.env.CLUSTER_USER, process.env.CLUSTER_PASSWORD);
var bucket = cluster.openBucket(process.env.BUCKET_NAME);

module.exports = app;
module.exports.bucket = bucket;

routes = require("./src/controllers/articleRoutes")(app);
routes = require("./src/controllers/categorieRoutes")(app);

